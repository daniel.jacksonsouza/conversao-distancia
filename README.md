# conversao-distancia
# Configuração do Dockerfile
FROM python:3.8 </br>
WORKDIR /app </br>
COPY requirements.txt . </br>
RUN python -m pip install -r requirements.txt </br>
COPY . . </br>
EXPOSE 5000 </br>
CMD ["gunicorn", "--workers=3", "--bind", "0.0.0.0:5000", "app:app"] </br>

